<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use App\Models\{
    User,
    Album,
    Photo,
    Source
};
use Illuminate\Database\Eloquent\Factories\Sequence;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        User::factory(6)->has(
            Album::factory()->count(4)->has(
                Photo::factory()->count(6)
                    ->state(new Sequence(
                        ['active' => true],
                        ['active' => false]
                    ))
                    ->has(Source::factory()->count(1))
            )
        )->create();
    }
}
