<?php

namespace Database\Factories;

use App\Models\Source;
use App\Models\Photo;
use Illuminate\Database\Eloquent\Factories\Factory;
use Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Source>
 */
class SourceFactory extends Factory
{
    protected $model = Source::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $with = 640;
        $height = 480;
        $path = $this->faker->image('public/storage/photos', $with, $height, null, true, true, true, false);
        return [
            //
            'photo_id'  => Photo::factory(),
            'path'      => $path,
            'url'       => config('app.url') . "/" . Str::after($path, 'public/'),
            'size'      => rand(1111, 9999),
            'width'     => $with,
            'height'    => $height,
        ];
    }
}
